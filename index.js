/**
 * Created by ksheppard on 2/10/2015.
 */


var fs = require("fs");
var fsEx = require("fs-extra");
var cp = require("child_process");
var readline = require("readline");
var path = require("path");

var args = process.argv.splice(2);

var cwd = process.cwd().replace(/\\/g, "/");

var homeDir = path.dirname(process.mainModule.filename).replace(/\\/g, "/");

var conf = require(homeDir + "/conf.json");

if(args.length > 0) {
    switch(args[0]) {
        case "edit":
            edit();
            break;
        case "help":
            console.log("Commands: \n" +
                "--------- \n" +
                "edit [app-id]      Launches the GUI to edit the application's settings. \n" +
                "help               Displays this menu \n" +
                "init               Creates a package.json file in the current directory. \n" +
                "install            Installs the app based on parameters in the package.json file in the current directory. \n" +
                "list               Lists all the applications installed. \n"+
                "monitor [app-id]   Launches a task tray utility to monitor the application status. \n" +
                "remove [app-id]    Uninstalls the application. \n" +
                "start [app-id]     Starts the application. \n" +
                "stop [app-id]      Stops the application. \n");
            break;
        case "init":
            init();
            break;
        case "install":
            install();
            break;
        case "list":
            list();
            break;
        case "monitor":
            monitor();
            break;
        case "remove":
            remove();
            break;
        case "start":
            start();
            break;
        case "stop":
            stop();
            break;
        default:
            console.log("Command not recognized: '"+args[0]+"'. Use playmgr help to get a list of all commands.");
            break;
    }
}

function appFound() {
    if(args.length > 1) {
        return fs.existsSync(getExecutable());
    }
    else {
        console.log("Insufficient arguments for this function.");
        return false;
    }
}

function getAppFolder() {
    return homeDir + "/apps/" + getAppName() + "/";
}

function getAppName() {
    return args[1].toLowerCase();
}

function getExecutable() {
    return getAppFolder() + getAppName() + ".exe";
}

function edit() {
    if(appFound()) {
        cp.exec(homeDir + "/procrun/prunmgr.exe //ES//"+getAppName());
    }
}

function init() {
    //get the current directory
    console.log("Writing install package...");
    //create a JSON object that has the arguments needed
    var installer = {
        serviceName: "myApp",
        displayName: "My Application",
        classPath: "lib/*",
        startPath: cwd,
        jvmOpts: "-Dhttp.port=9000;-Dpidfile.path=NUL;-Djava.net.preferIPv4Stack=true"
    };
    //save that JSON object as package.json in the app's directory
    fs.writeFile("package.json", JSON.stringify(installer, null, '\t'));
    console.log("Done! Edit the new package.json file then run 'playmgr install' when ready.");
}

function install() {
    fs.exists("./package.json", function(exists) {
        if(exists) {
            var installer = require(cwd+"/package.json");
            var appFolder = homeDir+"/apps/"+installer.serviceName.toLowerCase();
            if(!fs.existsSync(appFolder)) fsExtra.mkdirsSync(appFolder);
            var executable = appFolder+"/"+installer.serviceName.toLowerCase()+".exe";
            fsEx.copy(homeDir + conf.procrun, executable, function(err) {
                if(err) { console.log(err); }
                else {
                    var commandArgs =
                        "install " +
                        installer.serviceName + " " +
                        "--DisplayName=\""+installer.displayName+"\" "+
                        "--Install=\""+executable+"\" "+
                        "--Classpath=\""+installer.classPath+"\" "+
                        "--JvmOptions="+installer.jvmOpts + " "+
                        "--Jvm=\""+conf.jvm+"\" " +
                        "--StartPath=\""+installer.startPath+"\" "+
                        "--Startup=auto --StartClass=play.core.server.NettyServer "+
                        "--StartMode=jvm --StopClass=play.core.server.NettyServer --StopMode=jvm";
                    console.log(commandArgs);
                    cp.exec(executable + " " + commandArgs, function(error) {
                        if(error) console.log(error);
                    });
                }
            });

        } else console.log("Installer package not found in current directory. Please run 'playmgr init'.");
    });
}

function list() {
	if(fs.existsSync(homeDir + "/apps")) {
		var apps = fs.readdirSync(homeDir + "/apps");
		console.log(apps.join('\n'));
	} else {
		console.log("There are no applications installed.");
	}
    
}

function monitor() {
    if(appFound()) {
        cp.exec(homeDir + conf.procmanager+" //MS//"+getAppName());
    }
}

function remove() {
    if(appFound()) {
        cp.exec(getExecutable() + " delete " + getAppName(), function(error) {
            if(error) console.error(error);
            else {
                console.log("Uninstalled service. Removing directory...");
                fsEx.remove(getAppFolder(), function(error) {
                    if(error) console.error(error);
                    else console.log("Removed "+getAppFolder());
                });
            }

        });
    } else console.log("App with ID '"+getAppName()+"' not found.");
}

function start() {
    if(appFound()) {
        cp.exec(getExecutable() + " start " + getAppName());
    }
}

function stop() {
    if(appFound()) {
        cp.exec(getExecutable() + " stop " + getAppName());
    }
}
